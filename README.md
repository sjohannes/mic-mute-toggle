# mic-mute-toggle #

Tell PulseAudio to toggle the muted state of the default source, then tell
GNOME Shell to pop up an on-screen display showing the new state.

This emulates what happens when you press the XF86XK_AudioMicMute key in a
GNOME environment, without emulating the key press event itself.


## Why not just emulate the key press? ##

No particular reason; I just wanted to learn or refresh my knowledge on Vala,
Libpulse, GIO D-Bus, and Meson.

For reference, the `xdotool` equivalent to this app is

    xdotool key 0x1008FFB2
