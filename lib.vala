/* mic-mute-toggle
 * Copyright (C) 2017  Johannes Sasongko <sasongko@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public struct Mic {
	string label;
	/** Less than 0 if muted, otherwise between 0 and 100 */
	int level;
}

/** Toggles mic mute and returns the new state */
public Mic toggle_mute () {
	var mainloop = new PulseAudio.MainLoop ();
	var context = new PulseAudio.Context (mainloop.get_api (), "mic-mute-toggle");

	bool done = false;
	context.connect (null, PulseAudio.Context.Flags.NOAUTOSPAWN);
	while (!done) {
		mainloop.iterate ();
		switch (context.get_state ()) {
		case PulseAudio.Context.State.FAILED:
			error ("Failed connecting to PulseAudio server.");
		case PulseAudio.Context.State.READY:
			done = true;
			break;
		default:
			break;
		}
	}

	string source = null;
	done = false;
	context.get_server_info ((context, info) => {
		assert (info != null);
		source = info.default_source_name;
		done = true;
	});
	while (!done) {
		mainloop.iterate ();
	}

	bool mute = false;
	string description = null;
	float volume = 0;
	done = false;
	context.get_source_info_by_name (source, (context, info, eol) => {
		if (eol != 0) {
			done = true;
		} else {
			assert (info != null);
			mute = info.mute != 0;
			var port = info.active_port;
			assert (port != null);
			description = port.description;
			volume = (float) info.volume.avg () / (info.n_volume_steps - 1);
		}
	});
	while (!done) {
		mainloop.iterate ();
	}

	done = false;
	context.set_source_mute_by_name (source, !mute, (context, success) => {
		done = true;
	});
	while (!done) {
		mainloop.iterate ();
	}

	return {
		label: description,
		level: mute ? ((int) Math.lroundf (volume * 100)).clamp (0, 100) : -1,
	};
}

// XXX: Should really be internal.
public Variant get_osd_args (Mic mic) {
	string icon = (mic.level < 0) ? "microphone-sensitivity-muted-symbolic"
		: (mic.level < 34) ? "microphone-sensitivity-low-symbolic"
		: (mic.level < 67) ? "microphone-sensitivity-medium-symbolic"
		: "microphone-sensitivity-high-symbolic";
	return new Variant.parsed ("({"
		+ " 'icon': <%s>, 'label': <%s>, 'level': <%i> "
		+ "},)",
		icon, mic.label, (mic.level < 0) ? 0 : mic.level);
}

/** Displays GNOME Shell OSD showing mic level */
public void osd (Mic mic) throws Error {
	var bus = Bus.get_sync (BusType.SESSION);
	bus.call_sync ("org.gnome.Shell", "/org/gnome/Shell", "org.gnome.Shell",
		"ShowOSD", get_osd_args (mic), null, DBusCallFlags.NO_AUTO_START, -1);
}
