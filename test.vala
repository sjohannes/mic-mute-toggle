void assert_equal (string expected, string actual) {
	if (expected != actual) {
		error ("Assertion failed:\n** Expected: %s\n** Actual  : %s", expected, actual);
	}
}

void main () {
	var expected = "({'icon': <'microphone-sensitivity-muted-symbolic'>, 'label': <'Hello'>, 'level': <0>},)";
	var args = get_osd_args ({label: "Hello", level: -1});
	assert_equal (expected, args.print (false));

	expected = "({'icon': <'microphone-sensitivity-low-symbolic'>, 'label': <'Hello'>, 'level': <0>},)";
	args = get_osd_args ({label: "Hello", level: 0});
	assert_equal (expected, args.print (false));

	expected = "({'icon': <'microphone-sensitivity-medium-symbolic'>, 'label': <'Hello'>, 'level': <34>},)";
	args = get_osd_args ({label: "Hello", level: 34});
	assert_equal (expected, args.print (false));

	expected = "({'icon': <'microphone-sensitivity-high-symbolic'>, 'label': <'Hello'>, 'level': <67>},)";
	args = get_osd_args ({label: "Hello", level: 67});
	assert_equal (expected, args.print (false));
}
